package cmd

import "fmt"

func newInvalidModeError(mode Mode) error {
	return fmt.Errorf("invalid mode: %s", mode)
}
