package cmd

import (
	"go.uber.org/zap"
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab1/pkg/obfuscation"
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab1/pkg/utils"
)

type Mode string

const (
	OBF   Mode = "obf"
	DEOBF Mode = "deobf"
)

type App struct {
	obf    *obfuscation.Obfuscator
	logger *zap.Logger
}

func NewApp(logger *zap.Logger, shift int64) *App {
	obf := obfuscation.NewObfuscator(shift)
	return &App{
		obf:    obf,
		logger: logger,
	}
}

func (a *App) Run(mode Mode, pathSources ...string) error {
	a.logger.Info("run app",
		zap.String("mode", string(mode)),
		zap.Int("file queue", len(pathSources)))
	for _, pathSource := range pathSources {
		a.logger.Info("reading file", zap.String("filename", pathSource))
		data, err := utils.ReadFromFile(pathSource)
		if err != nil {
			return err
		}
		switch mode {
		case OBF:
			a.obf.Obfuscation(data)
		case DEOBF:
			a.obf.Deobfuscation(data)
		default:
			return newInvalidModeError(mode)
		}
		perms, err := utils.GetFilePerms(pathSource)
		if err != nil {
			return err
		}
		if err := utils.WriteToFile(pathSource, data, perms); err != nil {
			return err
		}
	}
	return nil
}
