.PHONY: deps
deps:
	go mod tidy

.PHONY: build
build:
	go build -o .output/service cmd/main.go

.PHONY: test
test:
	go test -cover ./...