# САБД - Lab 1 
## О задании
Name: Реализация программного средства для обфускирования и де-обфускирования данных

Author: Andrianov Artemii

Group: #5140904/30202

## Цель
Необходимо написать программу, которая может выполняться в одном из двух режимов - обфускирование и деобфускирование - 
в зависимости от получаемого параметра запуска. В качестве исходных данных, поступающих на вход программы, выступает XML-файл. 
На выходе необходимо получить измененный XML-файл. В режиме обфускирования исходный XML-файл считывается, 
обрабатывается и в результате работы программы создается новый обфусцированный XML-файл. 
С другой стороны в режиме деобфускирования созданный на предыдущем этапе XML-файл должен преобразоваться в исходный вариант.

## Решение
### Build
For build service use `make build`. `.output/service` will be created.

### Tests
For run tests use `make test`.

### Params
Flags:

| name  | default | description                                      |
|-------|---------|--------------------------------------------------|
| mode  | obf     | mode of service<br/>`obf` and `deobf` are enable |
| shift | 10      | size of shift for obfuscation                    |

For run service user:
```bash 
    ./output/service -mode=<your-mode> -shift=<your-shift> <files-paths-as-args-array>
```

