package main

import (
	"flag"
	"go.uber.org/zap"
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab1/internal/cmd"
)

func main() {
	mode := flag.String("mode", "obf", "mode of work")
	shift := flag.Int64("shift", 10, "shift size")
	flag.Parse()

	logger, _ := zap.NewProduction()
	defer logger.Sync()

	app := cmd.NewApp(logger, *shift)
	if err := app.Run(cmd.Mode(*mode), flag.Args()...); err != nil {
		logger.Fatal("error with work app", zap.Error(err))
	}
}
