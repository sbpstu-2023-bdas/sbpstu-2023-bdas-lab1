package obfuscation

type Obfuscator struct {
	shift int64
}

func NewObfuscator(shift int64) *Obfuscator {
	return &Obfuscator{
		shift: shift,
	}
}
