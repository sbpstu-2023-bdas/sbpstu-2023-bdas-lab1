package obfuscation

type IObfuscation interface {
	Obfuscation(data []byte) error
	Deobfuscation(data []byte) error
}
