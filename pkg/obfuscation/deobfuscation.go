package obfuscation

func (o *Obfuscator) Deobfuscation(data []byte) error {
	for i := 0; i < len(data); i++ {
		data[i] = byte(int64(data[i]) - o.shift)
	}
	return nil
}
