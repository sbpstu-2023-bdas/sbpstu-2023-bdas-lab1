package obfuscation

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestObfuscator_Obfuscation(t *testing.T) {
	suites := []struct {
		name     string
		source   []byte
		shift    int64
		expected []byte
	}{
		{
			name:     "test data A (eng)",
			source:   []byte("test data A"),
			shift:    10,
			expected: []byte("~o}~*nk~k*K"),
		},
		{
			name:     "test data B (ru)",
			source:   []byte("тестовые данные Б"),
			shift:    1,
			expected: []byte("҃Ѷ҂҃ѿѳҌѶ!ѵѱѾѾҌѶ!ђ"),
		},
	}

	for _, s := range suites {
		t.Run(s.name, func(t *testing.T) {
			obf := NewObfuscator(s.shift)
			d := s.source
			err := obf.Obfuscation(d)
			assert.NoError(t, err)
			assert.Equal(t, s.expected, d)
		})
	}
}

func TestObfuscator_Deobfuscation(t *testing.T) {
	suites := []struct {
		name     string
		source   []byte
		shift    int64
		expected []byte
	}{
		{
			name:     "test data A (eng)",
			source:   []byte("~o}~*nk~k*K"),
			shift:    10,
			expected: []byte("test data A"),
		},
		{
			name:     "test data B (ru)",
			source:   []byte("҃Ѷ҂҃ѿѳҌѶ!ѵѱѾѾҌѶ!ђ"),
			shift:    1,
			expected: []byte("тестовые данные Б"),
		},
	}

	for _, s := range suites {
		t.Run(s.name, func(t *testing.T) {
			obf := NewObfuscator(s.shift)
			d := s.source
			err := obf.Deobfuscation(d)
			assert.NoError(t, err)
			assert.Equal(t, s.expected, d)
		})
	}
}
