package utils

import (
	"github.com/stretchr/testify/assert"
	"os"
	"path"
	"testing"
)

const testDir = "../../test/testdata"

func TestGetFilePerms(t *testing.T) {

	cases := []struct {
		name          string
		filePath      string
		isErrorInvoke bool
		expectedStats os.FileMode
	}{
		{
			name:          "file exists",
			filePath:      path.Join(testDir, "source.txt"),
			isErrorInvoke: false,
			expectedStats: func() os.FileMode {
				stats, _ := os.Stat(path.Join(testDir, "source.txt"))
				return stats.Mode()
			}(),
		},
		{
			name:          "file not exists",
			filePath:      path.Join(testDir, "not-exists.txt"),
			isErrorInvoke: true,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			stats, err := GetFilePerms(c.filePath)
			if c.isErrorInvoke {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, c.expectedStats, stats)
			}
		})
	}
}

func TestReadFromFile(t *testing.T) {
	cases := []struct {
		name          string
		filePath      string
		isErrorInvoke bool
		expectedData  string
	}{
		{
			name:          "file exists",
			filePath:      path.Join(testDir, "source.txt"),
			isErrorInvoke: false,
			expectedData:  "testdata",
		},
		{
			name:          "file not exists",
			filePath:      path.Join(testDir, "not-exists.txt"),
			isErrorInvoke: true,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			data, err := ReadFromFile(c.filePath)
			if c.isErrorInvoke {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, c.expectedData, string(data))
			}
		})
	}
}

func TestWriteToFile(t *testing.T) {
	targetPath := path.Join(testDir, "target.txt")
	targetData := []byte("testdata")

	if err := WriteToFile(targetPath, targetData, 0666); err != nil {
		assert.NoError(t, err)
	}
	defer os.Remove(targetPath)

	resultData, _ := ReadFromFile(targetPath)
	assert.Equal(t, targetData, resultData)
}
