package utils

import "os"

func GetFilePerms(pathToFile string) (os.FileMode, error) {
	info, err := os.Stat(pathToFile)
	if err != nil {
		return 0, err
	}

	return info.Mode(), nil
}

func ReadFromFile(pathToFile string) ([]byte, error) {
	return os.ReadFile(pathToFile)
}

func WriteToFile(pathToFile string, data []byte, perms os.FileMode) error {
	return os.WriteFile(pathToFile, data, perms)
}
